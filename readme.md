#Nasz projekt testowy

To jest jakiś opis projektu:
- lista opcja 1
- lista opcja 2
- lista opcja 3
- lista opcja 4

## Procedura tworzenia repo:

1. Tworzymy katalog prejektu

    `mkdir nazwa`

2. W katalogu inicjujemy git-a
3. Tworzymy plik projektu
4. Dodajemy plik do śledzenia
5. Commitujemy plik do repozytorium
6. Tworzymy repozytorium zdalne
7. Podłączamy repo zdalne do lokalnego (synchroniacja)
8. Pushujemy lokalny branch master do zdalnego repo